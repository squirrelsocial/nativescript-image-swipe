import { CoercibleProperty, Property, ImageCache, ItemsSource, ScrollView } from "@nativescript/core/ui";
import { ImageSwipe as ImageSwipeDefinition } from ".";
export declare abstract class ImageSwipeBase extends ScrollView implements ImageSwipeDefinition {
    static pageChangedEvent: string;
    static _imageCache: ImageCache;
    items: any[] | ItemsSource;
    pageNumber: number;
    imageUrlProperty: string;
    isItemsSourceIn: boolean;
    allowZoom: boolean;
    constructor();
    _getDataItem(index: number): any;
    abstract refresh(): void;
}
export declare const pageNumberProperty: CoercibleProperty<ImageSwipeBase, number>;
export declare const itemsProperty: Property<ImageSwipeBase, any[] | ItemsSource>;
export declare const imageUrlPropertyProperty: Property<ImageSwipeBase, string>;
export declare const allowZoomProperty: Property<ImageSwipeBase, boolean>;
