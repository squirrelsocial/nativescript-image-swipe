import { ImageSwipeBase } from "./image-swipe-common";
export * from "./image-swipe-common";
export declare class ImageSwipe extends ImageSwipeBase {
    isScrollingIn: boolean;
    private _views;
    private _delegate;
    constructor();
    initNativeView(): void;
    onLoaded(): void;
    onUnloaded(): void;
    onLayout(left: number, top: number, right: number, bottom: number): void;
    refresh(): void;
    _centerImageView(imageView: UIImageView): void;
    private _loadCurrentPage;
    private _resizeNativeViews;
    private _loadPage;
    private _prepareImageView;
    private _positionImageView;
    private _purgePage;
    private _purgeAllPages;
    private _calcScrollViewContentSize;
}
