import { ImageSwipeBase } from "./image-swipe-common";
export * from "./image-swipe-common";
export declare class ImageSwipe extends ImageSwipeBase {
    nativeView: StateViewPager;
    createNativeView(): StateViewPager;
    initNativeView(): void;
    get android(): StateViewPager;
    refresh(): void;
}
declare class StateViewPager extends androidx.viewpager.widget.ViewPager {
    private _allowScrollIn;
    constructor(context: android.content.Context);
    onInterceptTouchEvent(event: android.view.MotionEvent): boolean;
    setAllowScrollIn(allowScrollIn: boolean): void;
}
